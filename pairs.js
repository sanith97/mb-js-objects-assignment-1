function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs

    const keyValuePairsList = [];

    for (const key in obj) {
        keyValuePairsList.push([key,obj[key]]);
    }

    return keyValuePairsList;
}

module.exports = pairs;