function defaults(obj = {}, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults

    const newObject = {};

    for (const keyOfDP in defaultProps) {

        (keyOfDP in obj) ? newObject[keyOfDP] = obj[keyOfDP] : newObject[keyOfDP] = defaultProps[keyOfDP];
    };    
        
    return newObject;
}

module.exports = defaults;