function keys(obj) {
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys

    const keysList = [];

    for (const key in obj) { //using for in loop as the argument passed is an object
        keysList.push(key);
    }

    return keysList;
}

module.exports = keys;