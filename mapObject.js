function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
    const newObject = {};

    for (const key in obj) {
        let returnedValue = cb(obj[key],key);
        newObject[key] = returnedValue;
    }

    return newObject;
};

module.exports = mapObject;

