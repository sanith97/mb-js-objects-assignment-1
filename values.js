function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values

    const valuesList = [];

    for (const key in obj) {
        valuesList.push(obj[key]);
    }

    return valuesList;
}

module.exports = values;